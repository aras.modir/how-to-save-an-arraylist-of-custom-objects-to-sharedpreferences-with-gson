package com.aras.modir.saveanarraylistwithshpr;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ExampleAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ArrayList<ExampleItem> mExamplelist;
    private Button buttonInsert1, buttonsave1;
    private EditText editTextInsert1, editTextInsert2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        loadData();
        buildRecyclerView();

        buttonInsert1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertItem();
            }
        });

        buttonsave1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mExamplelist);
        editor.putString("task list", json);
        editor.apply();
    }

    private void loadData (){
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("task list", null);
        Type type = new TypeToken<ArrayList<ExampleItem>>() {}.getType();
        mExamplelist = gson.fromJson(json, type);

        if (mExamplelist == null) {
            mExamplelist = new ArrayList<>();
        }
    }

    public void insertItem() {
        mExamplelist.add(new ExampleItem(editTextInsert1.getText().toString(),editTextInsert2.getText().toString()));
    }

    private void buildRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(mExamplelist);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void bind() {
        buttonInsert1 = findViewById(R.id.button_insert1);
        buttonsave1 = findViewById(R.id.button_save1);
        mRecyclerView = findViewById(R.id.recycleview);
        editTextInsert1 = findViewById(R.id.edittext_insert1);
        editTextInsert2 = findViewById(R.id.edittext_insert2);
    }
}
